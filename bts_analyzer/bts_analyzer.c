/*  
 * Copyright (C) 2004-2013  Lorenzo Pallara, l.pallara@avalpa.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02111-1307, USA.
 */

#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <inttypes.h>

#define TS_PACKET_SIZE 188
#define BTS_PACKET_SIZE 204
#define TMCC_SIZE 16
#define MAX_PID 8192
#define IIP_PID 8176
#define NULL_PID 8191

unsigned int last_frame_index = 0;
int pkts_A = 0;
int pkts_B = 0;
int pkts_C = 0;
int pkts_N = 0;

void print_tmcc(unsigned char *tmcc_buffer, unsigned short pid);

void print_modulation_control_configuration_information(unsigned char *pkt_buffer);

void print_iip(unsigned char *pkt_buffer);

void Usage(void);

int main(int argc, char *argv[])
{
	int byte_read;
	int fd_ts; /* File descriptor of ts file */
	unsigned short pid;
	unsigned int i;
	unsigned char *packet_buffer;
	unsigned char *tmcc_buffer;
	unsigned char *current_packet;
	unsigned int buffer_size;
	unsigned char pid_table[MAX_PID]; /* valid PID table */

	/* Parse input arguments */
	buffer_size = 0;
	memset(pid_table, 0, MAX_PID);
	if (argc >= 2)
	{
		fd_ts = open(argv[1], O_RDONLY);
		if (fd_ts < 0)
		{
			fprintf(stderr, "Can't find file %s\n", argv[1]);
			return 2;
		}
	}
	else
	{
		Usage();
		return 2;
	}
	i = 2;
	while (i < argc)
	{
		if (argv[i][0] == '+')
		{
			pid = atoi(&(argv[i][1]));
			if (pid < MAX_PID)
			{
				fprintf(stderr, "keep pid %d\n", pid);
				pid_table[pid] = 1;
				i++;
			}
			else
			{
				fprintf(stderr, "pid range should be from 0 to %d\n", MAX_PID);
				return 2;
			}
		}
		else
		{
			buffer_size = atoi(&(argv[i][0]));
			i++;
		}
	}
	if (buffer_size == 0)
	{
		buffer_size = 1;
	}

	/* Allocate packet buffer */
	buffer_size *= TS_PACKET_SIZE;
	packet_buffer = malloc(buffer_size);
	tmcc_buffer = malloc(16);

	if (packet_buffer == NULL)
	{
		fprintf(stderr, "Out of memory\n");
		return 2;
	}

	/* Start to process the file */
	byte_read = 1;
	while (byte_read)
	{

		/* read packets */
		byte_read = read(fd_ts, packet_buffer, buffer_size);
		byte_read = read(fd_ts, tmcc_buffer, 16);
		/* filter packets on their pids */
		for (i = 0; i < buffer_size; i += TS_PACKET_SIZE)
		{
			current_packet = packet_buffer + i;
			memcpy(&pid, current_packet + 1, 2);
			pid = ntohs(pid);
			pid = pid & 0x1fff;
			if (pid < MAX_PID)
			{
				//write(STDOUT_FILENO, current_packet, TS_PACKET_SIZE);
				print_tmcc(tmcc_buffer, pid);
				if (pid == 8176)
				{
					fprintf(stdout, "==== QUADRO ENCERRADO: \t%d PKTS A \t%d PKTS B \t%d PKTS C \t%d PKTS N     =======================================================================================\n", pkts_A, pkts_B, pkts_C, pkts_N);
					print_iip(packet_buffer);
					pkts_A = pkts_B = pkts_C = pkts_N = 0;
				}
			}
		}
	}

	return 0;
}

void print_tmcc(unsigned char *tmcc_buffer, unsigned short pid)
{
	unsigned char tmcc_id, reserv, buffer_reset_flag, emergency, mudanca, primeiro, par, layer, indice_mud, ac_sig, ac_bytes, frame_index_e, frame_index_d;
	tmcc_id = reserv = buffer_reset_flag = emergency = mudanca = primeiro = par = 0;

	char layer_identifier[10000];
	unsigned int frame_index;

	layer_identifier[0] = 'N';
	layer_identifier[1] = 'A';
	layer_identifier[2] = 'B';
	layer_identifier[3] = 'C';
	layer_identifier[8] = 'I';

	tmcc_id = tmcc_buffer[0] >> 6;
	reserv = (tmcc_buffer[0] & 0x20) >> 5;
	buffer_reset_flag = (tmcc_buffer[0] & 0x10) >> 4;
	emergency = (tmcc_buffer[0] & 0x08) >> 3;
	mudanca = (tmcc_buffer[0] & 0x04) >> 2;
	primeiro = (tmcc_buffer[0] & 0x02) >> 1;
	par = tmcc_buffer[0] & 0x01;

	layer = tmcc_buffer[1] >> 4;
	indice_mud = tmcc_buffer[1] & 0x0f;

	ac_sig = (tmcc_buffer[2]) >> 7;
	ac_bytes = (tmcc_buffer[2] & 0x60) >> 5;

	frame_index_e = (tmcc_buffer[2] & 0x1f);
	frame_index_d = tmcc_buffer[3];

	frame_index = frame_index_e << 8;
	frame_index |= frame_index_d;

	if ((last_frame_index != (frame_index - 1)) && (last_frame_index != 4351))
		fprintf(stdout, "||      Erro de continuidade no contador do quadro: quadro indice: %d, quadro indice anterior: %d \n", frame_index, last_frame_index);

	fprintf(stdout, "PID: %d  \t TMCC BYTES: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X | ID: %d  Reserved: %d  Buffer reset: %d  Emergencia: %d  Mudanca: %d  Primeiro: %d  Par: %d  | Layer: %c  Regressivo mudanca: %d  | AC Sig: %d AC Bytes: %d  | Indice do quadro: %d\n", pid, tmcc_buffer[0], tmcc_buffer[1], tmcc_buffer[2], tmcc_buffer[3], tmcc_buffer[4], tmcc_buffer[5], tmcc_buffer[6], tmcc_buffer[7], tmcc_buffer[8], tmcc_buffer[9], tmcc_buffer[10], tmcc_buffer[11], tmcc_buffer[12], tmcc_buffer[13], tmcc_buffer[14], tmcc_buffer[15], tmcc_id, reserv, buffer_reset_flag, emergency, mudanca, primeiro, par, layer_identifier[layer], indice_mud, ac_sig, ac_bytes, frame_index);

	if (layer == 0)
		pkts_N++;

	if (layer == 1)
		pkts_A++;

	if (layer == 2)
		pkts_B++;

	if (layer == 3)
		pkts_C++;

	last_frame_index = frame_index;
}

void print_modulation_control_configuration_information(unsigned char *pkt_buffer)
{
	unsigned char TMCC_synchronization_word, AC_data_effective_position, reserved, initialization_timing_indicator, current_mode, current_guard_interval, next_mode, next_guard_interval;

	//unsigned char current_mode,current_guard_interval,next_mode,next_guard_interval;

	unsigned char system_identifier, count_down_index, flag_alert_broadcasting, current_partial_reception_flag, next_partial_reception_flag;
	unsigned char A_modulation_scheme, A_coding_rate_of_inner_code, A_lenght_of_time_interleaving, A_number_of_segments;
	unsigned char B_modulation_scheme, B_coding_rate_of_inner_code, B_lenght_of_time_interleaving, B_number_of_segments;
	unsigned char C_modulation_scheme, C_coding_rate_of_inner_code, C_lenght_of_time_interleaving, C_number_of_segments;
	unsigned char phase_correction_of_CP_in_connected_transmission;
	unsigned int TMCC_reserved_future_use, reserved_future_use;

	unsigned int crc = 0;
	unsigned char crc_bytes[4];

	int i;

	char *str_guard_interval[7];
	char *str_modulation[7];
	char *str_inner_code[100];
	char *str_time_interleave[100];

	TMCC_synchronization_word = pkt_buffer[0] >> 7;
	AC_data_effective_position = (pkt_buffer[0] & 0x40) >> 6;
	reserved = (pkt_buffer[0] & 0x30) >> 4;
	initialization_timing_indicator = pkt_buffer[0] & 0x0F;

	current_mode = pkt_buffer[1] >> 6;
	current_guard_interval = (pkt_buffer[1] & 0x30) >> 4;
	next_mode = (pkt_buffer[1] & 0x0C) >> 2;
	next_guard_interval = pkt_buffer[1] & 0x03;

	str_guard_interval[0] = "1/32";
	str_guard_interval[1] = "1/16";
	str_guard_interval[2] = "1/8";
	str_guard_interval[3] = "1/4";

	str_modulation[0] = "DQPSK";
	str_modulation[1] = "QPSK";
	str_modulation[2] = "16QAM";
	str_modulation[3] = "64QAM";
	str_modulation[7] = " NA ";

	str_inner_code[0] = "1/2";
	str_inner_code[1] = "2/3";
	str_inner_code[2] = "3/4";
	str_inner_code[3] = "5/6";
	str_inner_code[4] = "7/8";
	str_inner_code[7] = "NA";

	str_time_interleave[0] = "0 (Modo 1), 0 (Modo 2) , 0 (Modo 3)";
	str_time_interleave[1] = "4 (Modo 1), 2 (Modo 2) , 1 (Modo 3)";
	str_time_interleave[2] = "8 (Modo 1) , 4 (Modo 2) , 2 (Modo 3)";
	str_time_interleave[3] = "16 (Modo 1) , 8 (Modo 2) , 4 (Modo 3)";
	str_time_interleave[7] = "NA";

	system_identifier = pkt_buffer[2] >> 6;
	count_down_index = (pkt_buffer[2] & 0x3c) >> 2;
	flag_alert_broadcasting = (pkt_buffer[2] & 0x02) >> 1;
	current_partial_reception_flag = (pkt_buffer[2] & 0x01);

	A_modulation_scheme = pkt_buffer[3] >> 5;
	A_coding_rate_of_inner_code = (pkt_buffer[3] & 0x1c) >> 2;

	A_lenght_of_time_interleaving = (pkt_buffer[3] & 0x03) << 1;
	A_lenght_of_time_interleaving |= pkt_buffer[4] >> 7;
	A_number_of_segments = (pkt_buffer[4] & 0x78) >> 3;

	B_modulation_scheme = pkt_buffer[4] & 0x07;
	B_coding_rate_of_inner_code = pkt_buffer[5] >> 5;
	B_lenght_of_time_interleaving = (pkt_buffer[5] & 0x1c) >> 2;

	B_number_of_segments = (pkt_buffer[5] & 0x03) << 2;
	B_number_of_segments |= pkt_buffer[6] >> 6;

	C_modulation_scheme = (pkt_buffer[6] & 0x38) >> 3;
	C_coding_rate_of_inner_code = pkt_buffer[6] & 0x07;
	C_lenght_of_time_interleaving = pkt_buffer[7] >> 5;
	C_number_of_segments = (pkt_buffer[7] & 0x1e) >> 1;

	next_partial_reception_flag = pkt_buffer[7] & 0X01;

	fprintf(stdout, "\t * 20 Bytes: modulation_control_configuration_information # ");
	for (i = 0; i < 20; i++)
		fprintf(stdout, " %02X", pkt_buffer[i]);
	fprintf(stdout, "\n");

	fprintf(stdout, "\t * \t # TMCC_synchronization_word: %d  AC_data_effective_position: %d  reserved: %d  initialization_timing_indicator: %d \n", TMCC_synchronization_word, AC_data_effective_position, reserved, initialization_timing_indicator);
	fprintf(stdout, "\t * \t # current_mode: %d  current_guard_interval: %s  next_mode: %d  next_guard_interval: %s\n", current_mode, str_guard_interval[current_guard_interval], next_mode, str_guard_interval[next_guard_interval]);
	fprintf(stdout, "\t * \t # system_identifier: %d  count_down_index: %d  flag_alert_broadcasting: %d  current_partial_reception_flag: %d\n", system_identifier, count_down_index, flag_alert_broadcasting, current_partial_reception_flag);
	fprintf(stdout, "\t * \t # A_modulation_scheme: %s \tA_coding_rate_of_inner_code: %s  A_lenght_of_time_interleaving %s  A_number_of_segments: %d\n", str_modulation[A_modulation_scheme], str_inner_code[A_coding_rate_of_inner_code], str_time_interleave[A_lenght_of_time_interleaving], A_number_of_segments);
	fprintf(stdout, "\t * \t # B_modulation_scheme: %s \tB_coding_rate_of_inner_code: %s  B_lenght_of_time_interleaving %s  B_number_of_segments: %d\n", str_modulation[B_modulation_scheme], str_inner_code[B_coding_rate_of_inner_code], str_time_interleave[B_lenght_of_time_interleaving], B_number_of_segments);
	fprintf(stdout, "\t * \t # C_modulation_scheme: %s \tC_coding_rate_of_inner_code: %s  C_lenght_of_time_interleaving %s  C_number_of_segments: %d\n", str_modulation[C_modulation_scheme], str_inner_code[C_coding_rate_of_inner_code], str_time_interleave[C_lenght_of_time_interleaving], C_number_of_segments);
	fprintf(stdout, "\t * \t # next_partial_reception_flag: %d\n", next_partial_reception_flag);

	A_modulation_scheme = pkt_buffer[8] >> 5;
	A_coding_rate_of_inner_code = (pkt_buffer[8] & 0x1c) >> 2;

	A_lenght_of_time_interleaving = (pkt_buffer[8] & 0x03) << 1;
	A_lenght_of_time_interleaving |= pkt_buffer[9] >> 7;
	A_number_of_segments = (pkt_buffer[9] & 0x78) >> 3;

	B_modulation_scheme = pkt_buffer[9] & 0x07;
	B_coding_rate_of_inner_code = pkt_buffer[10] >> 5;
	B_lenght_of_time_interleaving = (pkt_buffer[10] & 0x1c) >> 2;

	B_number_of_segments = (pkt_buffer[10] & 0x03) << 2;
	B_number_of_segments |= pkt_buffer[11] >> 6;

	C_modulation_scheme = (pkt_buffer[11] & 0x38) >> 3;
	C_coding_rate_of_inner_code = pkt_buffer[11] & 0x07;
	C_lenght_of_time_interleaving = pkt_buffer[12] >> 5;
	C_number_of_segments = (pkt_buffer[12] & 0x1e) >> 1;

	fprintf(stdout, "\t * \t # A_modulation_scheme: %s \tA_coding_rate_of_inner_code: %s  A_lenght_of_time_interleaving %s  A_number_of_segments: %d\n", str_modulation[A_modulation_scheme], str_inner_code[A_coding_rate_of_inner_code], str_time_interleave[A_lenght_of_time_interleaving], A_number_of_segments);
	fprintf(stdout, "\t * \t # B_modulation_scheme: %s \tB_coding_rate_of_inner_code: %s  B_lenght_of_time_interleaving %s  B_number_of_segments: %d\n", str_modulation[B_modulation_scheme], str_inner_code[B_coding_rate_of_inner_code], str_time_interleave[B_lenght_of_time_interleaving], B_number_of_segments);
	fprintf(stdout, "\t * \t # C_modulation_scheme: %s \tC_coding_rate_of_inner_code: %s  C_lenght_of_time_interleaving %s  C_number_of_segments: %d\n", str_modulation[C_modulation_scheme], str_inner_code[C_coding_rate_of_inner_code], str_time_interleave[C_lenght_of_time_interleaving], C_number_of_segments);

	phase_correction_of_CP_in_connected_transmission = (pkt_buffer[12] & 0x01) << 2;
	phase_correction_of_CP_in_connected_transmission |= pkt_buffer[13] >> 6;

	TMCC_reserved_future_use = (pkt_buffer[13] & 0x3f) << 6;
	TMCC_reserved_future_use |= (pkt_buffer[14] & 0xfc) >> 2;
	reserved_future_use = pkt_buffer[14] & 0x03;
	reserved_future_use |= pkt_buffer[15];
	fprintf(stdout, "\t * \t # phase_correction_of_CP_in_connected_transmission: %d TMCC_reserved_future_use: %d reserved_future_use: %d CRC: \n", phase_correction_of_CP_in_connected_transmission, TMCC_reserved_future_use, reserved_future_use);
	fprintf(stdout, "\t * \t # CRC: %02X%02X%02X%02X\n", pkt_buffer[16], pkt_buffer[17], pkt_buffer[18], pkt_buffer[19]);

	crc = sectioncrc(pkt_buffer, 16);

	crc_bytes[0] = (crc >> 24) & 0xFF;
	crc_bytes[1] = (crc >> 16) & 0xFF;
	crc_bytes[2] = (crc >> 8) & 0xFF;
	crc_bytes[3] = crc & 0xFF;
	//crc_32=crc32(crc_32, ( unsigned char*)pkt_buffer, 16);
	fprintf(stdout, "\t * \t # CRC: %X \n", crc);

	fprintf(stdout, "\t * \t # CRC: %02X%02X%02X%02X \n", crc_bytes[0], crc_bytes[1], crc_bytes[2], crc_bytes[3]);

	//crc_32 =
}

void print_iip(unsigned char *pkt_buffer)
{

	fprintf(stdout, "\t * Imprimindo IIP \n");

	unsigned char modulation_control_configuration_information[20], IIP_branch_number, last_IIP_branch_number, network_synchronization_information_lenght;

	unsigned int iip_pointer, stuf_bytes;

	int i;

	unsigned int crc = 0;

	iip_pointer = pkt_buffer[5];
	iip_pointer |= pkt_buffer[4] << 8;

	memcpy(modulation_control_configuration_information, pkt_buffer + 6, 20);

	IIP_branch_number = pkt_buffer[26];
	last_IIP_branch_number = pkt_buffer[27];
	network_synchronization_information_lenght = pkt_buffer[28];

	fprintf(stdout, "\t * 4 Bytes: %02X %02X %02X %02X do cabecalho TS\n", pkt_buffer[0], pkt_buffer[1], pkt_buffer[2], pkt_buffer[3]);

	fprintf(stdout, "\t * 2 Bytes: %02X %02X iip_pointer: %d \n", pkt_buffer[4], pkt_buffer[5], iip_pointer);

	print_modulation_control_configuration_information(modulation_control_configuration_information);

	fprintf(stdout, "\t * 2 Bytes: %02X %02X  IIP_branch_number: %d last_IIP_branch_number: %d\n", IIP_branch_number, last_IIP_branch_number, IIP_branch_number, last_IIP_branch_number);

	fprintf(stdout, "\t * 1 Byte:  %02X       network_synchronization_information_lenght: %d \n", network_synchronization_information_lenght, network_synchronization_information_lenght);

	if ((network_synchronization_information_lenght > 3) && (network_synchronization_information_lenght < 64))
	{
		unsigned int synchronization_time_stamp = 0;
		unsigned int sfn_max_delay = 0;

		unsigned int equipment_id, renewal_flag, static_delay_flag, reserved_future_use, time_offset_polarity, time_offset;
		synchronization_time_stamp = pkt_buffer[32];
		synchronization_time_stamp |= pkt_buffer[31] << 8;
		synchronization_time_stamp |= pkt_buffer[30] << 16;

		//fprintf(stdout,"\t * 1 Byte:  %02X       network_synchronization_information_lenght: %d \n",network_synchronization_information_lenght,network_synchronization_information_lenght);
		fprintf(stdout, "\t * \t SFN: synchronization_time_stamp: %d\n", synchronization_time_stamp);

		sfn_max_delay = pkt_buffer[35];
		sfn_max_delay |= pkt_buffer[34] << 8;
		sfn_max_delay |= pkt_buffer[33] << 16;

		fprintf(stdout, "\t * \t SFN: max delay: %d\n", sfn_max_delay);

		fprintf(stdout, "\t * \t SFN: extended bytes : %d\n", pkt_buffer[36]);

		if (pkt_buffer[36] > 0)
		{
			for (i = 0; i < pkt_buffer[36] / 5; i++)
			{

				equipment_id = pkt_buffer[38 + i * 5] >> 4;
				equipment_id |= pkt_buffer[37 + i * 5] << 4;

				renewal_flag = (pkt_buffer[38 + i * 5] & 0X08) >> 3;
				static_delay_flag = (pkt_buffer[38 + i * 5] & 0x04) >> 2;
				reserved_future_use = (pkt_buffer[38 + i * 5] & 0x02) >> 1;
				time_offset_polarity = pkt_buffer[38 + i * 5] & 0x01;

				time_offset = pkt_buffer[41 + i * 5];
				time_offset |= pkt_buffer[40 + i * 5] << 8;
				time_offset |= pkt_buffer[39 + i * 5] << 16;

				fprintf(stdout, "\t * \t SFN: \t  equipment_id: %d renewal_flag: %d static_delay_flag: %d reserved_future_use: %d time_offset_polarity: %d time_offset: %d \n", equipment_id, renewal_flag, static_delay_flag, reserved_future_use, time_offset_polarity, time_offset);
			}
		}

		crc = sectioncrc(&pkt_buffer[30], network_synchronization_information_lenght - 5);

		fprintf(stdout, "\t * \t SFN: \t # CRC: %X \n", crc);
	}

	stuf_bytes = 159 - network_synchronization_information_lenght;

	int ai;
	fprintf(stdout, "\t * %d Stuffing Bytes\n", stuf_bytes);
	for (ai = 0; ai < 159; ai++)
	{

		fprintf(stdout, " %02X", pkt_buffer[29 + ai]);
	}

	fprintf(stdout, "\t * \n");
}

void Usage(void)
{
	fprintf(stderr, "Usage: 'tsfilter filename.ts +pid1 +pid2 ... +pidn [buffer_size_in_packets]'\n");
	fprintf(stderr, "+pid keeps 'pid' packets\n");
}