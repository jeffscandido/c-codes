// Example of reading a file one byte at a time
// and display it in hex format on the terminal

#include <stdio.h>

int main()
{
    unsigned char c, tsp_buffer[204]; // Using char, because sizeof(char) == 1 byte
    unsigned short int j = 0, tsp = 0;

    FILE *fp = fopen("BTStest.ts", "r"); // open the file in 'read' mode

    while (!feof(fp))
    {
        if (tsp < 10)
        {
            printf("\n\nTSP number: %d", tsp);
        }
        for (j = 0; j < 204; j++)
        {
            tsp_buffer[j] = fgetc(fp); // get a character/byte from the file
            if (tsp < 10)
            {
                printf("\nByte[%d]: 0x%02X", j, tsp_buffer[j]); // and show it in hex format
            }                                                 // while not end of file
        }
        tsp++;
    }
    fclose(fp); // close the file
    return 0;
}