// Example of reading a file one byte at a time
// and display it in hex format on the terminal

#include <stdio.h>

int main()
{
    unsigned char c, tspBuffer[204]; // Using char, because sizeof(char) == 1 byte
    unsigned short int i = 0, j = 0, PID = 0, PIDvector[8192] /*, tspHeader = 0*/;
    unsigned char teiTspHeader = 0, pusiTspHeader = 0, tpTspHeader = 0;
    unsigned char tscTspHeader = 0, afcTspHeader = 0, ccTspHeader = 0;

    FILE *fp = fopen("BTStest.ts", "r"); // open the file in 'read' mode
    for (i = 0; i < 8192; i++)
    {
        PIDvector[i] = 0;
    }

    while (!feof(fp))
    {

        for (j = 0; j < 204; j++)
        {
            tspBuffer[j] = fgetc(fp); // get a character/byte from the file
        }
        PID = ((tspBuffer[1] & 31) << 8) | tspBuffer[2]; // Packet Identifier (PID)
        PIDvector[PID]++;
    }
    for (i = 0; i < 8192; i++)
    {
        if (PIDvector[i] != 0)
        {
            printf("PID numero %d foi usado %d vezes\n", i, PIDvector[i]);
        }
    }
    fclose(fp); // close the file
    return 0;
}