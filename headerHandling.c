// Example of reading a file one byte at a time
// and display it in hex format on the terminal

#include <stdio.h>

int main()
{
    unsigned char c, tspBuffer[204]; // Using char, because sizeof(char) == 1 byte
    unsigned short int j = 0, tsp = 0, PID = 0 /*, tspHeader = 0*/;
    unsigned char transport_error_indicator = 0, payload_unit_start_indicator = 0, transport_priority = 0;
    unsigned char transport_scrambling_control = 0, adaptation_field_control = 0, continuity_counter = 0;

    FILE *fp = fopen("BTStest.ts", "r"); // open the file in 'read' mode

    while (!feof(fp))
    {

        for (j = 0; j < 204; j++)
        {
            tspBuffer[j] = fgetc(fp); // get a character/byte from the file
            //if (tsp < 10)
            //{
            //  printf("\nByte[%d]: %02X", j, tspBuffer[j]); // and show it in hex format
            //}                                                 // while not end of file
        }
        //if (tsp < 100)
        //{
        //tspHeader = (tspBuffer[1] << 8) | tspBuffer[2];
        //printf("\n\nTSP number: %d\n", tsp);
        //printf("\nByte[1]: %02X", tspBuffer[1]);
        //printf("\nByte[2]: %02X", tspBuffer[2]);
        transport_error_indicator = (tspBuffer[1] & 128) >> 7;    // Transport error indicator (TEI)
        payload_unit_start_indicator = (tspBuffer[1] & 64) >> 6;  // Payload unit start indicator (PUSI)
        transport_priority = (tspBuffer[1] & 32) >> 5;            // Transport priority
        PID = ((tspBuffer[1] & 31) << 8) | tspBuffer[2];          // Packet Identifier (PID)
        transport_scrambling_control = (tspBuffer[3] & 192) >> 6; //Transport scrambling control (TSC)
        adaptation_field_control = (tspBuffer[3] & 48) >> 4;      // Adaptation field control
        continuity_counter = tspBuffer[3] & 15;                   // Continuity counter

        switch (adaptation_field_control)
        {
        case 0:
            printf("Reserved for future use by ISO/IEC\n");
            break;
        case 1:
            printf("No adaptation_field, payload only\n");
            break;

        case 2:
            printf("Adaptation_field only, no payload\n");

            /*

            Variables of Adaption Field
            adaptation_field_length
            discontinuity_indicator
            random_access_indicator
            elementary_stream_priority_indicator
            PCR_flag
            OPCR_flag
            splicing_point_flag
            transport_private_data_flag
            adaptation_field_extension_flag
            program_clock_reference_base
            reserved
            program_clock_reference_extension
            original_program_clock_reference_base
            reserved
            original_program_clock_reference_extension
            splice_countdown
            transport_private_data_length
            private_data_byte
            adaptation_field_extension_length
            ltw_flag
            piecewise_rate_flag
            seamless_splice_flag
            af_descriptor_not_present_flag
            reserved
            ltw_valid_flag
            ltw_offset
            reserved
            piecewise_rate
            Splice_type
            DTS_next_AU[32..30]
            marker_bit
            DTS_next_AU[29..15]
            marker_bit
            DTS_next_AU[14..0]
            marker_bit
            reserved
            stuffing_byte
            
            */
            break;

        case 3:
            printf("Adaptation_field followed by payload\n");
            break;

        default:
            printf("Invalid adaptation_field_control!\n");
        }

        if (adaptation_field_control == 2 || adaptation_field_control == 3)
        {
            printf("\n\nTSP number: %d\n", tsp);
            printf("\tTEI\tPUSI\tTP\tPID\t\tTSC\tAFC\tCC\n");
            printf("\t%d\t%d\t\t%d\t%04d\t%d\t%d\t%d\n", transport_error_indicator, payload_unit_start_indicator, transport_priority, PID, transport_scrambling_control, adaptation_field_control, continuity_counter);
        }

        //}
        tsp++;
    }
    fclose(fp); // close the file
    return 0;
}