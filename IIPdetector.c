// Example of reading a file one byte at a time
// and display it in hex format on the terminal

#include <stdio.h>

void isdbt_information_packet();

int main()
{
    unsigned char c, tspBuffer[204]; // Using char, because sizeof(char) == 1 byte
    unsigned short int j = 0, tsp = 0, PID = 0 /*, tspHeader = 0*/;
    unsigned char transport_error_indicator = 0, payload_unit_start_indicator = 0, transport_priority = 0;
    unsigned char transport_scrambling_control = 0, adaptation_field_control = 0, continuity_counter = 0;

    unsigned short int IIP_packet_pointer = 0, IIP_branch_number = 0, last_IIP_branch_number = 0;
    unsigned short int network_synchronization_information_length = 0;
    unsigned char TMCC_synchronization_word = 0, AC_data_effective_position = 0, initialization_timing_indicator = 0;
    unsigned char current_mode = 0, current_guard_interval = 0, next_mode = 0, next_guard_interval = 0;

    unsigned char modulation_scheme_A = 0,coding_rate_of_inner_code_A = 0, length_of_time_interleaving_A = 0, number_of_segments_A = 0;
    unsigned char modulation_scheme_B = 0,coding_rate_of_inner_code_B = 0, length_of_time_interleaving_B = 0, number_of_segments_B = 0;
    unsigned char modulation_scheme_C = 0,coding_rate_of_inner_code_C = 0, length_of_time_interleaving_C = 0, number_of_segments_C = 0;

    unsigned char partial_reception_flag = 0;

    FILE *fp = fopen("BTStest.ts", "r"); // open the file in 'read' mode

    while (!feof(fp))
    {

        for (j = 0; j < 204; j++)
        {
            tspBuffer[j] = fgetc(fp);
        }

        transport_error_indicator = (tspBuffer[1] & 128) >> 7;    // Transport error indicator (TEI)
        payload_unit_start_indicator = (tspBuffer[1] & 64) >> 6;  // Payload unit start indicator (PUSI)
        transport_priority = (tspBuffer[1] & 32) >> 5;            // Transport priority
        PID = ((tspBuffer[1] & 31) << 8) | tspBuffer[2];          // Packet Identifier (PID)
        transport_scrambling_control = (tspBuffer[3] & 192) >> 6; //Transport scrambling control (TSC)
        adaptation_field_control = (tspBuffer[3] & 48) >> 4;      // Adaptation field control
        continuity_counter = tspBuffer[3] & 15;                   // Continuity counter

        // Inside Payload Data

        IIP_packet_pointer = (tspBuffer[4] << 8) | tspBuffer[5];

        // Inside Modulation Configuration and Control Information - 20 bytes
        // vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

        TMCC_synchronization_word = tspBuffer[6] & 128 >> 7;  // 1 bit
        AC_data_effective_position = tspBuffer[6] & 128 >> 6; // 1 bit
        // reserved = tspBuffer[6] & 48 >> 4;                 // 2 bits

        // mode_GI_information
        initialization_timing_indicator = tspBuffer[6] & 15; // 4 bits
        current_mode = tspBuffer[7] & 192 >> 5;              // 2 bits
        current_guard_interval = tspBuffer[7] & 48 >> 4;     // 2 bits
        next_mode = tspBuffer[7] & 12 >> 2;                  // 2 bits
        next_guard_interval = tspBuffer[7] & 3;              // 1 bit

        // TMCC_information
        // transmission_parameters_for_layer_A
        modulation_scheme_A = (tspBuffer[9] & 224) >> 5;                                          // 3 bits
        coding_rate_of_inner_code_A = (tspBuffer[9] & 25) >> 2;                                   // 3 bits
        length_of_time_interleaving_A = ((tspBuffer[9] & 3) << 1) | ((tspBuffer[10] & 128) >> 7); // 3 bits
        number_of_segments_A = (tspBuffer[10] & 120) >> 3;                                        // 4 bits

        // transmission_parameters_for_layer_B
        modulation_scheme_B = tspBuffer[10] & 7;                                          // 3 bits
        coding_rate_of_inner_code_B = (tspBuffer[11] & 224) >> 5;                         // 3 bits
        length_of_time_interleaving_B = (tspBuffer[11] & 25) >> 2;                        // 3 bits
        number_of_segments_B = ((tspBuffer[11] & 3) << 2) | ((tspBuffer[12] & 192) >> 5); // 4 bits

        // transmission_parameters_for_layer_C
        modulation_scheme_C = (tspBuffer[12] & 56) >> 3;            // 3 bits
        coding_rate_of_inner_code_C = tspBuffer[12] & 7;            // 3 bits
        length_of_time_interleaving_C = (tspBuffer[13] & 224) >> 5; // 3 bits
        number_of_segments_C = (tspBuffer[13] & 30) >> 1;          // 4 bits

        // next_configuration_information
        partial_reception_flag = tspBuffer[13] & 1;

        // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        IIP_branch_number = tspBuffer[26];
        last_IIP_branch_number = tspBuffer[27];
        network_synchronization_information_length = tspBuffer[28];

        // switch (adaptation_field_control)
        // {
        // case 0:
        //     printf("Reserved for future use by ISO/IEC\n");
        //     break;
        // case 1:
        //     printf("No adaptation_field, payload only\n");
        //     break;

        // case 2:
        //     printf("Adaptation_field only, no payload\n");
        //     break;

        // case 3:
        //     printf("Adaptation_field followed by payload\n");
        //     break;

        // default:
        //     printf("Invalid adaptation_field_control!\n");
        // }

        if (tsp < 200)
        {
            printf("\n\nTSP number: %d\n", tsp);
            printf("\tTEI\tPUSI\tTP\tPID\t\tTSC\tAFC\tCC\n");
            printf("\t%d\t%d\t\t%d\t%04d\t%d\t%d\t%d\n", transport_error_indicator, payload_unit_start_indicator, transport_priority, PID, transport_scrambling_control, adaptation_field_control, continuity_counter);
        }
        tsp++;
    }
    fclose(fp); // close the file
    return 0;
}