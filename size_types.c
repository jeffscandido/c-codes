#include <stdio.h>
#include <stdbool.h>

int main()
{
    unsigned short int intType;
    float floatType;
    double doubleType;
    unsigned char charType;
    bool booleano;
    // sizeof evaluates the size of a variable
    printf("Size of unsigned short int: %ld bytes\n", sizeof(intType));
    printf("Size of float: %ld bytes\n", sizeof(floatType));
    printf("Size of double: %ld bytes\n", sizeof(doubleType));
    printf("Size of char: %ld byte\n", sizeof(charType));
    printf("Size of bool: %ld byte\n", sizeof(booleano));

    return 0;
}